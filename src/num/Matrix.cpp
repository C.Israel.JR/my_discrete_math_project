#include <num/Matrix.hpp>

bool Matrix::set(int i, int j, float val){
  if( i< MAX_ROWS && j< MAX_COLS){
    mat[i][j] = val;
    return true;
  }  
  return false;
}

bool Matrix::get(int i, int j, float &val){
  if( i< MAX_ROWS && j< MAX_COLS){
    val = mat[i][j];
    return true;
  }  
  return false;
}


float Matrix::get(int i, int j){
  return mat[i][j];
}



void Matrix::print(){
  for(int i=0; i < _rows; ++i){
    for(int j=0; j < _cols; ++j)
      cout << mat[i][j] << " ";
  
    cout << endl;
  }
  cout << endl;
} 
