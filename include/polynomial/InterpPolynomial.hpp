//MSE 2022-III INFOTEC Matemáticas Discretas  
//Prof. Daniel A. Cervantes
//Tarea 2. Unidad 1. Relaciones y Funciones.

#include <iostream>
#include <num/Matrix.hpp>
#include <polynomial/Polynomial.hpp>

using namespace std;

class InterpPolynomial: public Polynomial{

private:
  Matrix van;
  Matrix coeff;
  
private:
  void buildVandermonde(float[],float[]);
  void gaussE();

public:
  InterpPolynomial(){};
  InterpPolynomial(float[],float[],int);
  float eval(float);
  
  //Polynomial(){nMonomials = 0;} 
  //void addMonomial(char var, int exp); // Daniel
  //float evalue(float);     //Todos
  //void add(const Polynomial& pol1, Polynomial& out_pol2); //Todos 
  //void diff(const Polynomial& pol1, Polynomial& out_pol2); //Todos
  //void mult(const Polynomial& pol1, Polynomial& out_pol2); //Todos
  //void div(const Polynomial& pol1, Polynomial& out_pol2);
  //void derivate(Polynomial& out_pol2);
  //float integral();
  //void graph(int eval);   
  //void print(); //Daniel  
};

