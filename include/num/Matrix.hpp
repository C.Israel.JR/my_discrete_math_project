//MSE 2022-III INFOTEC Matemáticas Discretas  
//Prof. Daniel A. Cervantes
//Tarea 3.

#include <iostream>
#include<cstdlib>

using namespace std;
#define MAX_ROWS  100
#define MAX_COLS  100


class Matrix {
  float mat[MAX_ROWS][MAX_COLS];              // Arreglo bidimensional
  int  _rows;                                 // No. de renglones
  int  _cols;                                 // No. de columnas     

public:
  
  int rows(){return _rows;}
  int cols(){return _cols;}
  void rows(int rows){_rows = rows;}
  void cols(int cols){_cols = cols;}
  
  Matrix(int n, int m){_rows = n; _cols = m;}
  Matrix(){_rows = 0; _cols = 0;}

  bool set(int,int,float);
  bool get(int,int,float &);
  float get(int,int);
  void add(const Matrix &, Matrix &);  //Implementar
  void sMult(float, Matrix &);         //Implemetar, multiplicación por escalar 
  void diff(const Matrix&, Matrix &);  //Implementar, diferencia matricial.
  void mult(const Matrix&, Matrix &);  //Implementar, multiplicación matricial
  float det();                         //Implemntar, determinante de la matriz
  void eGaussian(Matrix &,Matrix &);    //Implementar, eliminación Guassiana
  void print();
};


// bool Matrix::set(int i, int j, float val){
//   if( i< MAX_ROWS && j< MAX_COLS){
//     mat[i][j] = val;
//     return true;
//   }  
//   return false;
// }

// bool Matrix::get(int i, int j, float &val){
//   if( i< MAX_ROWS && j< MAX_COLS){
//     val = mat[i][j];
//     return true;
//   }  
//   return false;
// }

// void Matrix::print(){
//   for(int i=0; i < _rows; ++i){
//     for(int j=0; j < _cols; ++j)
//       cout << mat[i][j] << " ";
  
//     cout << endl;
//   }
//   cout << endl;
// } 
