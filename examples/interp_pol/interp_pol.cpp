/*Matematicas Discretas MSE-III 2022 INFOTEC
  Tarea 4: Interpolacion polinomial
  Prof. Daniel A. Cervantes
  Diciembre 2022
*/

#include <polynomial/InterpPolynomial.hpp>
#include <utils/GNUplot.hpp>
#include <utils/WriteFile.hpp>

int main(){

  GNUplot plotter;
  WriteFile writter;
  int pause;

  string interp_data_file = "./data/interp_data.txt";
  string eval_data_file = "./data/eval_data.txt";
  
  float x[] = {1,5,10,40};
  float y[] = {56.5,113.0,181.0,214.5};
  int degree = 3;

  int neval = 100;
  float eval_x[neval+1];
  float eval_y[neval+1];
 
  // InterpPolynomial ip(x,y,degree);

  // for(int i=0; i <= neval; ++i){
  //   eval_x[i] = x[0] + (x[degree]-x[0])*(float(i)/neval);
  //   eval_y[i] = ip.eval(eval_x[i]);
  // }

  // writter.write(interp_data_file,x,y,degree+1);
  // writter.write(eval_data_file,eval_x,eval_y,neval+1);
  
  string s = "set title"+string("\'")+"Polynomial Interpolation of degree "+ to_string(degree) + string("\'") +";"+"plot "+string("\'")+ interp_data_file+string("\'") + "using 1:2 w p lw 5 title 'interp data' ," +string("\'")+eval_data_file+string("\'") + "using 1:2 w l lw 3 title 'evaluation'";
  plotter(s.c_str());
  pause = cin.get();
  
  return 0;
}
